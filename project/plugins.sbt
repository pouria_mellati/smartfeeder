addSbtPlugin("com.hanhuy.sbt" % "android-sdk-plugin" % "1.3.16")

addSbtPlugin("com.hanhuy.sbt" % "sbt-idea" % "1.7.0-SNAPSHOT")  // The plugin from mpeltonen/sbt-idea can be used once  pr #314 is merged into it.

resolvers += Resolver.sbtPluginRepo("snapshots")  // for the above plugin
