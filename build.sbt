name := "SmartFeeder"

scalaVersion := "2.11.5"

javacOptions += "-Xlint:deprecation"

proguardOptions in Android ++= Seq(
  "-dontwarn scala.**",
  "-dontwarn scalaz.**"
)

resolvers ++= Seq(
  "jcenter" at "http://jcenter.bintray.com",
  "spray" at "http://repo.spray.io/"
)

val scalazVersion = "7.1.1"
val androidVersion = "21.0.3"

libraryDependencies ++= Seq(
  aar("com.android.support" % "appcompat-v7" % androidVersion),
  aar("com.android.support" % "support-v4" % androidVersion),
  "org.scalaz" %% "scalaz-core" % scalazVersion,
  "org.scalaz" %% "scalaz-concurrent" % scalazVersion,
  aar("org.macroid" %% "macroid" % "2.0.0-M3"),
  "com.github.kevinsawicki" % "http-request" % "6.0",
  "io.spray" %%  "spray-json" % "1.3.1"
)
