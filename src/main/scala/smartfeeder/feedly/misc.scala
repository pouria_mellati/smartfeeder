package smartfeeder.feedly

import android.content.Context
import smartfeeder.android.R

object FeedlyConstants {
  final val FEEDLY_API_URL = "http://sandbox.feedly.com"
  final val FEEDLY_CLIENT_ID = "sandbox"

  // This will be a normal constant once we get a real (non-sandbox) client secret. So the android
  // dependency (the Context param) can be removed.
  def FEEDLY_CLIENT_SECRET(implicit ctx: Context) = ctx getString R.string.feedly_sandbox_secret
}

case class FeedlyOAuthCredentials(
  userId: String,
  accessToken: String,
  accessTokenExpiryDateSeconds: Long,
  refreshToken: Option[String] = None)