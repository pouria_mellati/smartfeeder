package smartfeeder.ai

case class ScoredPhrase(phrase: String, score: Double)

trait KeyphraseExtractor {
  def keyphrasesIn(text: String): Iterable[ScoredPhrase]
}

trait AppealingPhrasesStore {
  def appealOf(phrase: String): Double
}

class TextAppealScorer(kpExtractor: KeyphraseExtractor, phraseAppeals: AppealingPhrasesStore) {
  def score(text: String): Double =
    kpExtractor.keyphrasesIn(text).map {case ScoredPhrase(phrase, keynessScore) =>
      keynessScore * phraseAppeals.appealOf(phrase)
    }.sum
}