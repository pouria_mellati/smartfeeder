package smartfeeder.ai

import scala.collection.mutable.ArrayBuffer
import scala.math.log

trait DocsStatistics {
  def numDocsContaining(phrase: String): Int
  def totalNumDocs: Int
}

class TfIdfKeyphraseExtractor(docsStats: DocsStatistics) extends KeyphraseExtractor {
  override def keyphrasesIn(text: String): Iterable[ScoredPhrase] = {
    val (termFreqs, bigramFreqs, trigramFreqs) = phraseFrequencies(text)

    val scoredPhrases = new ArrayBuffer[ScoredPhrase](termFreqs.size + bigramFreqs.size + trigramFreqs.size)

    val numDocs = docsStats.totalNumDocs

    for((phraseSet, scoreMultiplier) <- List(termFreqs -> 1.0,
                                             bigramFreqs -> 1.5,
                                             trigramFreqs -> 2.0))
      for((phrase, countInThisTxt) <- phraseSet)
        scoredPhrases +=
          ScoredPhrase(phrase, tfIdfScore(countInThisTxt, docsStats numDocsContaining phrase, numDocs) * scoreMultiplier)

    scoredPhrases.sortBy(_.score) takeRight 4  // TODO: Can be done without sorting.
  }

  private def phraseFrequencies(text: String) = {
    val tokens = text split """\W+"""

    def phraseCounts(xs: Iterator[Array[String]]): collection.Map[String, Int] = {
      val freqs = collection.mutable.HashMap.empty[String, Int]
      for(x <- xs) {
        val phrase = x mkString " "
        freqs get phrase match {
          case None => freqs(phrase) = 1
          case Some(currCount) => freqs(phrase) = currCount + 1
        }
      }
      freqs
    }

    val termFreqs = phraseCounts(tokens sliding 1)
    val bigramFreqs  = phraseCounts(tokens sliding 2) filter {case (_, count) => count >= 2}
    val trigramFreqs = phraseCounts(tokens sliding 3) filter {case (_, count) => count >= 2}

    (termFreqs, bigramFreqs, trigramFreqs)
  }

  private def tfIdfScore(countInTxt: Int, numContainingDocs: Int, totalNumDocs: Int): Double = {
    if(numContainingDocs < 1)
      throw new IllegalArgumentException("numContainingDocs cannot be smaller than 1.")

    val sublinnearTf = if(countInTxt > 0) 1 + log(countInTxt) else 0
    val idf = log(totalNumDocs.toDouble / numContainingDocs)

    sublinnearTf * idf
  }
}