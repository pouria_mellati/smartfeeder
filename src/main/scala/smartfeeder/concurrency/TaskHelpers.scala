package smartfeeder.concurrency

import scalaz.\/
import scalaz.concurrent.Task
import java.util.concurrent.{Future => _, _}
import android.os.Handler
import android.os.Looper

object TaskHelpers {
  protected trait ThreadPoolCreationHelpers {
    final val ThreadTimeoutSecs = 30
  
    val defaultThreadFactory = Executors.defaultThreadFactory 
    
    def threadFactory(daemon: Boolean) = new ThreadFactory {
      override def newThread(r: Runnable) = {
        val t = defaultThreadFactory.newThread(r)
        t setDaemon daemon
        t
      }
    } 
    
    /** Creates cached TPs with an upper bound for the number of threads. */ 
    def cachedThreadPoolWith(maxThreads: Int, threadFactory: ThreadFactory = defaultThreadFactory) = {
      val tp = new ThreadPoolExecutor(maxThreads, maxThreads, ThreadTimeoutSecs, TimeUnit.SECONDS,
        new LinkedBlockingQueue[Runnable], threadFactory)
      tp allowCoreThreadTimeOut true
      tp
    }
  }
  
  object ExecutionContexts extends ThreadPoolCreationHelpers {
    /** For cpu intensive code. Ideally, I/O code should not run on this. */
    val cpu = Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors)
    
    /** For waiting over network requests. */
    val nw = cachedThreadPoolWith(maxThreads = 30, threadFactory(daemon = true))
    
    /** For concurrent access to local storage. */
    val storage = cachedThreadPoolWith(maxThreads = 5)
    
    /** Android UI thread as an `ExecutorService`. */
    val ui = new AbstractExecutorService {
      private lazy val androidUiHandler = new Handler(Looper.getMainLooper)
      
      override def execute(r: Runnable) {androidUiHandler post r}
      override def awaitTermination(timeout: Long, tUnit: TimeUnit): Boolean = ???
      override def isShutdown(): Boolean = false
      override def isTerminated(): Boolean = false
      override def shutdown() {???}
      override def shutdownNow() = ???
    }
  }
  
  import ExecutionContexts._
  
  def TaskCpu[A](a: => A) = Task(a)(cpu)
  def TaskNw[A](a: => A) = Task(a)(nw)
  def TaskStrg[A](a: => A) = Task(a)(storage)
  
  implicit class TaskExecutionContextSwitchOps[A](self: Task[A]) {
    /** Map using `f` in the supplied `ExecutorService`. */
    def mapIn[B](executor: ExecutorService, f: A => B) =
      self flatMap {a => Task(f(a))(executor)}
    
    /** Runs `f` & the non-async prefix of its resulting `Task` in the supplied `ExecutorService`. */
    def flatMapIn[B](executor: ExecutorService, f: A => Task[B]) =
      self flatMap {a => Task.fork(f(a))(executor)}
    
    /** Runs this `Task` to completion & then executes `handle` in the supplied `ExecutorService`. */
    def runAsyncIn(executor: ExecutorService, handle: Throwable \/ A => Unit) =
      self runAsync {result => Task(handle(result))(executor).runAsync(_ => ())}
    
    def mapCpu[B](f: A => B): Task[B] = mapIn(cpu, f)
    def flatMapCpu[B](f: A => Task[B]): Task[B] = flatMapIn(cpu, f)
    
    def runAsyncUi(handler: Throwable \/ A => Unit) = runAsyncIn(ui, handler)
  }
}