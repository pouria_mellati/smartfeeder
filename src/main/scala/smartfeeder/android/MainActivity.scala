package smartfeeder.android

import android.app.Activity
import android.os.Bundle
import android.widget.{LinearLayout, Button, TextView}
import macroid._
import macroid.FullDsl._

class MainActivity extends Activity with Contexts[Activity] {
  /** Called when the activity is first created. */
  override def onCreate(savedInstanceState: Bundle) = {
    super.onCreate(savedInstanceState)

    var textView = slot[TextView]

    def updatedTextViewText =
      if(MainActivityData.numClicks > 0)
        text(s"Tapped ${MainActivityData.numClicks} times!")
      else
        text("Tap the button!")

    setContentView {
      getUi {
        l[LinearLayout](
          w[Button] <~
            text("The Button") <~
            On.click {
              MainActivityData.numClicks += 1
              textView <~ updatedTextViewText
            },
          w[TextView] <~
            wire(textView) <~
            updatedTextViewText
        ) <~ (landscape ? horizontal | vertical)
      }
    }
  }
}

object MainActivityData {
  var numClicks = 0
}