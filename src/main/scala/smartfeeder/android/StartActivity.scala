package smartfeeder.android

import android.app.{Activity, AlertDialog}
import android.content.{Context, DialogInterface, Intent}
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.{KeyEvent, View}
import android.webkit.{WebChromeClient, WebView, WebViewClient}
import android.widget.{ProgressBar, TextView}
import com.github.kevinsawicki.http.HttpRequest
import macroid.FullDsl._
import macroid._
import scalaz.concurrent.Task
import scalaz._
import smartfeeder.concurrency.TaskHelpers._
import smartfeeder.feedly.{FeedlyConstants, FeedlyOAuthCredentials}
import smartfeeder.json.JsonConverters._
import smartfeeder.net.HttpRequestHelpers._
import smartfeeder.storage.android.FeedlyOAuthCredentialsStore
import spray.json._


/** The first activity that is started for the app. Will usually redirect to another appropriate
  * activity based on, for instance whether the user has oAuth data stored or if internet is
  * available.
  */
class StartActivity extends Activity with Contexts[Activity] {
  import FeedlyConstants._

  override def onCreate(savedInstanceState: Bundle) = {
    super.onCreate(savedInstanceState)

    if(FeedlyOAuthCredentialsStore.storedCredentialsExists) {
      if(isConnectedToTheInternet) {
        refreshAccessTokenIfNecessaryAndStartMainActivity
      } else {
        setContentView(getUi{
          w[TextView] <~ text("Offline mode is not yet available!")
        })
      }
    } else {
      if(isConnectedToTheInternet) {
        initiateFeedlyOAuth
      } else {
        tellUserToRestartWithInternet
      }
    }
  }

  private implicit val _: Context = this

  private final val OAUTH_REDIRECT_URI = "http://localhost"

  def isConnectedToTheInternet = {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager]
    val activeNetwork = cm.getActiveNetworkInfo
    activeNetwork != null && activeNetwork.isConnectedOrConnecting
  }

  def tellUserToRestartWithInternet {
    setContentView(getUi{
      w[TextView] <~
          text(
            "You are not connected to the internet and have no saved profile.\n\n" +
            "Please restart the app once you are connected to the internet.") <~
          padding(all = 20)
    })
  }

  var keyDownHandler = {(keyCode: Int, event: KeyEvent) => super.onKeyDown(keyCode, event)}
  override def onKeyDown(keyCode: Int, event: KeyEvent) = keyDownHandler(keyCode, event)

  def initiateFeedlyOAuth {
    setTitle("Connect To Feedly")
    setContentView(R.layout.feedly_login)

    val webView = findViewById(R.id.loginWebView).asInstanceOf[WebView]
    val progressBar = findViewById(R.id.loginProgressBar).asInstanceOf[ProgressBar]

    val wvSettings = webView.getSettings
    wvSettings setJavaScriptEnabled true
    wvSettings setBuiltInZoomControls true
    wvSettings setSupportZoom true

    val loginUrl = FEEDLY_API_URL + "/v3/auth/auth?response_type=code&scope=https://cloud.feedly.com/subscriptions" +
      s"&client_id=$FEEDLY_CLIENT_ID" +
      s"&redirect_uri=$OAUTH_REDIRECT_URI"
    webView loadUrl loginUrl

    webView setWebViewClient new WebViewClient {
      override def shouldOverrideUrlLoading(view: WebView, url: String): Boolean = {
        val urlWithAuthResult = s"""$OAUTH_REDIRECT_URI.*[?&](code|error)=([^&]*).*""".r
        urlWithAuthResult findFirstIn url match {
          case Some(urlWithAuthResult("code", authCode)) =>
            handleNewFeedlyOAuthCodeAndStartMainActivity(authCode)
          case Some(urlWithAuthResult("error", errorMsg)) =>
            setContentView(getUi{
              w[TextView] <~
                text(s"Login failed:\n\n$errorMsg") <~
                padding(all = 20)
            })
          case _ =>
            view loadUrl url
        }
        true
      }
    }

    webView setWebChromeClient new WebChromeClient {
      override def onProgressChanged(view: WebView, progress: Int) {
        progressBar.setVisibility(if(progress <= 0 || progress >= 100) View.GONE
          else View.VISIBLE)

        progressBar setProgress progress
      }
    }

    keyDownHandler = {(keyCode: Int, event: KeyEvent) =>
      if(keyCode != KeyEvent.KEYCODE_BACK)
        super.onKeyDown(keyCode, event)
      else {
        if(webView.canGoBack)
          webView.goBack
        else {
          new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Quit?")
            .setMessage("Quit the App?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener {
              override def onClick(dialog: DialogInterface, which: Int) {finish}})
            .setNegativeButton("No", null)
            .show
        }
        true
      }
    }
  }

  def handleNewFeedlyOAuthCodeAndStartMainActivity(grantCode: String) {
    setContentView(R.layout.authentication_wait)

    exchangeFeedlyOAuthCodeForAccessToken(grantCode) runAsyncUi {
      case \/-(oAuthCreds) =>
        FeedlyOAuthCredentialsStore store oAuthCreds
        startMainActivity
      case -\/(e) =>
        setContentView(getUi {
          w[TextView] <~ text(s"Feedly authentication failed!\n${e.getMessage}")
        })
    }
  }

  def exchangeFeedlyOAuthCodeForAccessToken(grantCode: String): Task[FeedlyOAuthCredentials] = {
    def tryGetAccessToken(attemptsLeft: Int = 3): Task[String] =
      exec { HttpRequest.post(s"$FEEDLY_API_URL/v3/auth/token").formData(
        "code" -> grantCode,
        "client_id" -> FEEDLY_CLIENT_ID,
        "client_secret" -> FEEDLY_CLIENT_SECRET,
        "redirect_uri" -> OAUTH_REDIRECT_URI,
        "state" -> "",
        "grant_type" -> "authorization_code")
      } flatMapCpu {resp =>
        if(resp.ok)
          Task now resp.body
        else if(attemptsLeft == 1)
          Task fail new Exception(s"Exchanging feedly OAuth grant code for access token failed with http code ${resp.code}.")
        else tryGetAccessToken(attemptsLeft - 1)
      }

    tryGetAccessToken() mapCpu {jsonStr => jsonStr.parseJson.convertTo[FeedlyOAuthCredentials]}
  }

  def refreshAccessTokenIfNecessaryAndStartMainActivity {
    FeedlyOAuthCredentialsStore.retrieve match {
      case None =>
        throw new IllegalStateException("Expected stored FeedlyAuthData.")
      case Some(authData) =>
        if(!isAccessTokenRefreshNeeded(authData.accessTokenExpiryDateSeconds))
          startMainActivity
        else {
          setContentView(R.layout.authentication_wait)

          exec { HttpRequest.post(s"$FEEDLY_API_URL/v3/auth/token").formData(
            "refresh_token" -> authData.refreshToken.getOrElse {
              throw new IllegalStateException("Expected a stored feedly refresh token.")},
            "client_id" -> FEEDLY_CLIENT_ID,
            "client_secret" -> FEEDLY_CLIENT_SECRET,
            "grant_type" -> "refresh_token")
          } runAsyncUi {
            case \/-(resp) =>
              import java.net.HttpURLConnection._
              if (resp.ok) {
                val newAuthData = resp.body.parseJson.convertTo[FeedlyOAuthCredentials]
                FeedlyOAuthCredentialsStore store newAuthData
                startMainActivity
              } else if (Seq(HTTP_FORBIDDEN, HTTP_UNAUTHORIZED) contains resp.code)
                initiateFeedlyOAuth
              else sys.error(s"Unexpected status code for feedly access token refresh request: ${resp.code}")
            case -\/(t) => throw t  // TODO: Tell user couldn't reach feedly.
          }
        }
    }
  }
  
  private def isAccessTokenRefreshNeeded(accessTokenExpiryDateInSeconds: Long) = {
    val minHoursOfAccessTknValidityRequiredForNoRefresh = 12
    val earliestDateInSecsRequiredForAccessTknValidityForNoRefresh =
      System.currentTimeMillis / 1000 + minHoursOfAccessTknValidityRequiredForNoRefresh * 3600
    accessTokenExpiryDateInSeconds < earliestDateInSecsRequiredForAccessTknValidityForNoRefresh
  }
  
  def startMainActivity {
    startActivity(new Intent(this, classOf[MainActivity]))
  }
}