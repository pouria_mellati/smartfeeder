package smartfeeder.net

import com.github.kevinsawicki.http.HttpRequest
import smartfeeder.concurrency.TaskHelpers._
import scala.collection.JavaConverters._
import scalaz.concurrent.Task

/** Helpers for using the HttpRequest lib easily & safely. */
object HttpRequestHelpers {

  /** A safer type for representing the response of an HttpRequest.
    *
    * For instance, calling body() on a raw HttpRequest more than once will result in
    * either an exception or an empty string, whereas calling body() multiple times on
    * this type is safe.
    *
    * Also hides the "request sending" parts of the API of HttpRequest.
    */
  class HttpResponse(underlying: HttpRequest) {
    // New methods can be added as required.
    def code = underlying.code
    def ok = underlying.ok
    lazy val body = underlying.body
    def headers = underlying.headers
  }

  /** Takes a block of code that will result in a HttpRequest. Ensures that the request
    * is actually executed in the network execution context, and returns a safer object
    * (than the HttpRequest) for representing the "response".
    */
  def exec(reqBuilder: => HttpRequest): Task[HttpResponse] = TaskNw {
    val req = reqBuilder
    req.code  // Force the request to execute right now.
    new HttpResponse(req)
  }

  implicit class PimpedHttpRequest(self: HttpRequest) {
    def formData(vals: (String, String)*) =
      self.form(vals.toMap.asJava)
  }
}