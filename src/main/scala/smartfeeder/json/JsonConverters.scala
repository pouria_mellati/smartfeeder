package smartfeeder.json

import smartfeeder.feedly.FeedlyOAuthCredentials
import spray.json._

object JsonConverters extends DefaultJsonProtocol {
  implicit val feedlyAuthDataReader = new JsonReader[FeedlyOAuthCredentials] {
    override def read(jsVal: JsValue): FeedlyOAuthCredentials =
      jsVal.asJsObject.getFields("id", "access_token", "expires_in") match {
        case Seq(JsString(userId), JsString(accessTkn), JsNumber(expirySecsFromNow)) =>
          val expiryDateInSecs = System.currentTimeMillis / 1000 + expirySecsFromNow.toLong
          val refreshTknOpt = jsVal.asJsObject.fields.get("refresh_token") map {_.convertTo[String]}
          FeedlyOAuthCredentials(userId, accessTkn, expiryDateInSecs, refreshTknOpt)
        case _ => throw new DeserializationException(s"Failed to deserialize to FeedlyAuthData from:\n${jsVal.prettyPrint}")
      }
  }
}
