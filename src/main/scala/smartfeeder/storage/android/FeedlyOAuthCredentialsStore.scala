package smartfeeder.storage.android

import android.content.Context
import smartfeeder.feedly.FeedlyOAuthCredentials

import scala.util.Try

object FeedlyOAuthCredentialsStore {
  private final val PREFS_FILE_NAME = "feedlyAuthData"
  private final val USER_ID_PREF_NAME = "userId"
  private final val ACCESS_TOKEN_PREF_NAME = "accessToken"
  private final val ACCESS_TOKEN_EXPIRY_DATE_SECS_PREF_NAME = "accessTokenExpDateSecs"
  private final val REFRESH_TOKEN_PREF_NAME = "refreshToken"

  /** The refreshToken in the returned object will never be None. */
  def retrieve(implicit ctx: Context): Option[FeedlyOAuthCredentials] = for {
    prefs <- Try(ctx.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE)).toOption
    userId <- Option(prefs.getString(USER_ID_PREF_NAME, null))
    accessTkn <- Option(prefs.getString(ACCESS_TOKEN_PREF_NAME, null))
    accessTknExpDateSecs <- Option(prefs.getLong(ACCESS_TOKEN_EXPIRY_DATE_SECS_PREF_NAME, -1)) filterNot {_ == -1}
    refreshTkn <- Option(prefs.getString(REFRESH_TOKEN_PREF_NAME, null))
  } yield FeedlyOAuthCredentials(userId, accessTkn, accessTknExpDateSecs, Some(refreshTkn))

  def storedCredentialsExists(implicit ctx: Context) = retrieve(ctx).isDefined

  def store(creds: FeedlyOAuthCredentials)(implicit ctx: Context) {
    if(!creds.refreshToken.isDefined && (!this.storedCredentialsExists || !this.retrieve.get.refreshToken.isDefined))
      throw new IllegalArgumentException("Cannot store a FeedlyAuthData without a refresh token, if no refresh token pre-exists.")

    val editor = ctx.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE).edit
    editor.putString(USER_ID_PREF_NAME, creds.userId)
    editor.putString(ACCESS_TOKEN_PREF_NAME, creds.accessToken)
    editor.putLong(ACCESS_TOKEN_EXPIRY_DATE_SECS_PREF_NAME, creds.accessTokenExpiryDateSeconds)
    creds.refreshToken.foreach{editor.putString(REFRESH_TOKEN_PREF_NAME, _)}
    editor.commit
  }
}
